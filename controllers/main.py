# -*- coding: utf-8 -*-
# Copyright 2019 Le Filament - <maintenance@le-filament.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import re
from cStringIO import StringIO

from odoo import http, fields
from odoo.http import request
from odoo.tools.misc import xlwt
from odoo.addons.web.controllers.main import content_disposition
from odoo.addons.web.controllers.main import serialize_exception


class BoxtalExporter(http.Controller):
    def export_xls(self, lignes_export, header, filename_):
        workbook = xlwt.Workbook(encoding='utf-8')
        worksheet = workbook.add_sheet(filename_)

        for i, fieldname in enumerate(header):
            worksheet.write(0, i, fieldname)
            worksheet.col(i).width = 8000  # around 220 pixels

        base_style = xlwt.easyxf('align: wrap yes')

        for row_index, line in enumerate(lignes_export):
            for cell_index, h in enumerate(header):
                cell_style = base_style
                if line[cell_index] is False:
                    cell_value = u''
                elif isinstance(line[cell_index], basestring):
                    cell_value = re.sub("\r", " ", line[cell_index])
                else:
                    cell_value = line[cell_index]
                worksheet.write(row_index + 1, cell_index,
                                cell_value, cell_style)

        fp = StringIO()
        workbook.save(fp)
        fp.seek(0)
        data = fp.read()
        fp.close()

        filename = filename_ + '.xls'

        csvhttpheaders = [
            ('Content-Type', 'text/csv;charset=utf8'),
            ('Content-Disposition', content_disposition(filename)),
        ]

        return request.make_response(data, headers=csvhttpheaders)

    @http.route('/web/export_boxtal/', type='http', auth="user")
    @serialize_exception
    def export_boxtal(self, sale_id, **kwargs):
        header = [
            'Civilité', 'Nom', 'Prénom', 'Société', 'Adresse', 'Complément',
            'Suite', 'Code postal', 'Ville', 'Etat ou province format ISO',
            'Pays format ISO', 'Téléphone', 'Email',
            'Informations importantes sur l\'adresse', 'Catégorie du contenu',
            'Description détaillée du contenu', 'Quantité', 'Poids (kg)',
            'Largeur (cm)', 'Longueur (cm)', 'Hauteur (cm)', 'Valeur',
            'Code point relais livraison', 'Référence interne',
        ]
        line = []
        filename_ = ""
        sale_ids = request.env['sale.order'].search([['id', '=', sale_id]])
        for s in sale_ids:
            line.append([
                s.partner_id.title.shortcut, s.partner_id.lastname,
                s.partner_id.firstname, s.partner_id.parent_id.name,
                s.partner_shipping_id.street, s.partner_shipping_id.street2,
                '', s.partner_shipping_id.zip, s.partner_shipping_id.city,
                '', 'FR', s.partner_id.phone, s.partner_id.email, '',
                'Objets et tableaux courants',
                'Refuges en bois pour petits animaux',
                '1', '', '', '', '', str(int(s.amount_total)),
                s.partner_shipping_id.x_shortid_mondialrelay, s.name
            ])
            filename_ = ('ExportBoxtal_' + s.partner_id.lastname
                         + '_' + fields.Date.today().replace('-', ''))

        return self.export_xls(line, header, filename_)
