# -*- coding: utf-8 -*-
# Copyright 2019 Le Filament - <maintenance@le-filament.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, api


class BoxtalSale(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def get_export_boxtal(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/export_boxtal?sale_id=%s' % (self.id,),
            'context': '{ active_id: %s, }' % (self.id,),
            'target': 'new'
            }
