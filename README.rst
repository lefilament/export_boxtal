.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


=============
Export Boxtal
=============

Description
===========

Ce module permet d'exporter les informations d'une commande pour les importer dans l'outil d'import `Boxtal <https://www.boxtal.com/fr/fr/app/centrale-expeditions/import-fichier>`_..

Credits
=======

Contributors ------------

* Benjamin Rivier <benjamin@le-filament.com>

Maintainer ----------

.. image:: https://le-filament.com/img/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
