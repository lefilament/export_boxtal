# -*- coding: utf-8 -*-
# Copyright 2019 Le Filament - <maintenance@le-filament.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    'name': "Export Boxtal",

    'summary': """
        Export Excel to Boxtal""",
    'author': "LE FILAMENT",
    'website': "https://le-filament.com",
    'license': 'AGPL-3',
    'version': '0.1',
    'contributors': [
        'Benjamin Rivier <benjamin@le-filament.com>',
    ],
    'depends': ['sale', 'partner_firstname'],
    'data': [
        # 'security/ir.model.access.csv',
        'views/sale.xml',
    ],
}
